# ESIT Training 
Lab 03: Updating and committing Containers 

---

## Preparations

 - Clean your docker host using the commands (in PowerShell):

```
$ docker rm -f $(docker ps -a -q)
```


## Docker Commit

 - Run the application (version 3.0) in a Docker container (detached mode) in the port 5000 using:
```
$ docker run -d -p 3000:80 --name static-app nginx:latest
```

 - Ensure the container is running:
```
$ docker ps
```

 - Create a new file into the container using the commands:
```
$ docker exec -it static-app /bin/bash
$ echo "MY ReadMe Content " > DontMoveWithoutThis
$ ls -l
$ cat DontMoveWithoutThis
$ exit
```

 - Create a new image including the changes:
```
$ docker commit static-app nginx:es
```

 - Delete the container static-app-3.0 using:
```
$ docker rm -f static-app
```

 - Inspect the images
```
$ docker images
```

 - Run a new container from the Original version image (in port 3000):
```
$ docker run -d -p 3000:80 --name static-app nginx:latest
```

 - Run a container from the created image (in port 3001):
```
$ docker run -d -p 3001:80 --name static-app-es nginx:es
```

 - Inspect the running containers with:
```
$ docker ps
```

 - Inspect the filesystem of the Original version container:
```
$ docker exec static-app ls -l
```

 - Inspect the filesystem of the commited container:
```
$ docker exec static-app-es ls -l
```



